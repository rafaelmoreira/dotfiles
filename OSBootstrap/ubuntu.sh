#!/usr/bin/env bash
sudo apt-get update
sudo apt-get install -y \
  git-core	\
  curl		\
  wget		\
  build-essential \
  software-properties-common \
  tklib \
  zlib1g-dev \
  libssl-dev \
  libreadline-gplv2-dev \
  libxml2 \
  libxml2-dev \
  libxslt1-dev \
  sqlite3 \
  libsqlite3-dev
